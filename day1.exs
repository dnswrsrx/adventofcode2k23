# Re-filling some of the letters to account for overlap e.g. sevenine -> 79
replacements = %{
  "one" => "o1e",
  "two" => "2o",
  "three" => "3e",
  "four" => "4",
  "five" => "5e",
  "six" => "6",
  "seven" => "7n",
  "eight" => "e8t",
  "nine" => "n9e"
}

first_and_last = &(List.to_integer([List.first(&1), List.last(&1)]))

collect_integer = &(
  String.to_charlist(&1)
  |> Enum.filter(fn char -> char in ?0..?9 end)
  |> first_and_last.()
)

numberify = &(
  Enum.reduce(replacements, &1, fn (replacement, line) ->
    { string, number } = replacement
     String.replace(line, string, number)
  end)
)

sample = [
  "1abc2",
  "pqr3stu8vwx",
  "a1b2c3d4e5f",
  "treb7uchet"
]

IO.puts(sample |> Stream.map(collect_integer) |> Enum.sum() === 142)

sample = [
  "two1nine",
  "eightwothree",
  "abcone2threexyz",
  "xtwone3four",
  "4nineeightseven2",
  "zoneight234",
  "7pqrstsixteen",
]

IO.puts(sample |> Stream.map(numberify) |> Stream.map(collect_integer) |> Enum.sum() == 281)

content = File.read!("inputs/day1.txt") |> String.split("\n", trim: true)
IO.puts(content |> Stream.map(collect_integer) |> Enum.sum() === 53334)
IO.puts(content |> Stream.map(numberify) |> Stream.map(collect_integer) |> Enum.sum() === 52834)
